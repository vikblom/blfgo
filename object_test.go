package blfgo

import (
	"bytes"
	"math/rand"
	"testing"
)

func TestMarshal(t *testing.T) {
	// marhsall/unmarshall on random bytes should be no-op.
	for i := 0; i < 64; i++ {
		input := make([]byte, 32)
		_, err := rand.Read(input)
		if err != nil {
			t.Error(err)
		}

		var head objectHead
		head.unmarshal(input)
		output := head.marshal()

		if !bytes.Equal(input, output) {
			t.Fatalf("Mismatch after roundtrip!\nExpected:%v\nRecieved:%v",
				input, output)
		}
	}
}

func TestReadWrite(t *testing.T) {

}
