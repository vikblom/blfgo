package blfgo

import (
	"bytes"
	"io"
	"math/rand"
	"testing"
)

func TestEmptyReaders(t *testing.T) {
	rd := NewContainerReader(bytes.NewReader(make([]byte, 0)))

	out := make([]byte, 0)
	n, _ := rd.Read(out)
	if n > 0 || len(out) > 0 {
		t.Fatal("Incorrectly Read empty into empty!")
	}

	out2 := make([]byte, 8)
	n, err := rd.Read(out2)
	if !(err == io.EOF && n == 0) {
		t.Fatalf("Empty read, expected EOF, got %v %v", n, err)
	}
}

func TestInvalidBacking(t *testing.T) {
	// This is not a correct byte sequence, should fail
	back := bytes.NewReader(make([]byte, 256))
	rd := NewContainerReader(back)

	out := make([]byte, 8)
	_, err := rd.Read(out)
	if err == nil {
		t.Fatal("Read on invalid bytes passed, expected error.")
	}
}

func TestSingleLobj(t *testing.T) {

	bw := bytes.NewBuffer(make([]byte, 0))
	if err := writeContainer(bw, []byte{0, 1, 2, 3}, false); err != nil {
		t.Fatal("writeContainer failed!")
	}

	out := make([]byte, 4)
	cr := NewContainerReader(bw)
	n, err := cr.Read(out)
	if n != 4 || err != nil {
		t.Fatalf("Expected (4, nil), got (%v, %v)", n, err)
	}
	for i := range out {
		if byte(i) != out[i] {
			t.Fatalf("Expected %d got %d", i, out[i])
		}
	}
}

func TestMultipleLobj(t *testing.T) {
	bw := bytes.NewBuffer(make([]byte, 0))
	if err := writeContainer(bw, []byte{0, 1, 2, 3}, false); err != nil {
		t.Fatal("writeContainer failed!")
	}
	if err := writeContainer(bw, []byte{4, 5, 6, 7}, false); err != nil {
		t.Fatal("writeContainer failed!")
	}

	cr := NewContainerReader(bw)
	out, err := io.ReadAll(cr)
	if len(out) != 8 || err != nil {
		t.Fatalf("Expected ([0..7], nil), got (%v, %v)", out, err)
	}
	for i := range out {
		if byte(i) != out[i] {
			t.Fatalf("Expected %d got %d", i, out[i])
		}
	}
}

func TestCompressedLobj(t *testing.T) {
	input := make([]byte, 1024)
	_, err := rand.Read(input)
	if err != nil {
		t.Error(err)
	}
	bw := bytes.NewBuffer([]byte{})

	if err := writeContainer(bw, input, true); err != nil {
		t.Fatal("writeContainer failed!")
	}

	cr := NewContainerReader(bw)
	out, err := io.ReadAll(cr)
	if err != nil {
		t.Fatal("Unexpected error from Read():", err)
	}
	if len(out) != len(input) {
		t.Fatalf("Read %d bytes but expected %d", len(out), len(input))
	}

}

func TestUnalignedLobjs(t *testing.T) {
	bw := bytes.NewBuffer(make([]byte, 0))
	// LOBJs with len % 4 != 0
	// total 11 bytes
	if err := writeContainer(bw, []byte{1, 1, 1, 1, 1, 1}, false); err != nil {
		t.Fatal("writeContainer failed!")
	}
	if err := writeContainer(bw, []byte{2, 2, 2, 2, 2}, false); err != nil {
		t.Fatal("writeContainer failed!")
	}

	cr := NewContainerReader(bw)
	out, err := io.ReadAll(cr)
	if len(out) != 11 || err != nil {
		t.Fatalf("Expected (11, nil), got (%v, %v)", len(out), err)
	}
}
