// Binary Log Format routines
//
// Vectors proprietary format.
// Implementations exist in various open source projects:
// https://github.com/hardbyte/python-can/blob/develop/can/io/blf.py
//
package blfgo

// type BlfCanMsg struct {
// 	Channel uint16
// 	Flags   uint8
// 	DLC     uint8
// 	ID      uint32
// 	Data    [8]byte
// } // 16

type CanMessage struct {
	Channel uint16
	ID      uint32
	Data    []byte
}
