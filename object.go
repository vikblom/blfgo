package blfgo

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
)

const (
	blfObjCanMessage  = 1
	blfObjCanMessage2 = 86
)

type objectHead struct {
	// Typical fields for BLF
	// Signature
	Signature [4]byte
	// Bytes in base+optinal extra header
	Headsize uint16
	// Version always 1
	Version uint16
	// Bytes in object (including header)
	Objsize uint32
	// Objtype
	Objtype uint32

	// Extra fields for objects
	Flags uint32
	// ???
	ClientIndex uint16
	// Decides timestamp format etc.
	ObjVersion uint16
	// Time of sample
	Timestamp uint64
}

// unmarshal objectHead from bytes bs.
func (h *objectHead) unmarshal(bs []byte) {

	copy(h.Signature[:], bs)
	h.Headsize = binary.LittleEndian.Uint16(bs[4:6])
	h.Version = binary.LittleEndian.Uint16(bs[6:8])
	h.Objsize = binary.LittleEndian.Uint32(bs[8:12])
	h.Objtype = binary.LittleEndian.Uint32(bs[12:16])
	h.Flags = binary.LittleEndian.Uint32(bs[16:20])
	h.ClientIndex = binary.LittleEndian.Uint16(bs[20:22])
	h.ObjVersion = binary.LittleEndian.Uint16(bs[22:24])
	h.Timestamp = binary.LittleEndian.Uint64(bs[24:32])
}

// marshal objectHead into bytes.
func (h *objectHead) marshal() []byte {

	bs := make([]byte, 32)
	copy(bs[0:4], h.Signature[:])
	binary.LittleEndian.PutUint16(bs[4:6], h.Headsize)
	binary.LittleEndian.PutUint16(bs[6:8], h.Version)
	binary.LittleEndian.PutUint32(bs[8:12], h.Objsize)
	binary.LittleEndian.PutUint32(bs[12:16], h.Objtype)
	binary.LittleEndian.PutUint32(bs[16:20], h.Flags)
	binary.LittleEndian.PutUint16(bs[20:24], h.ClientIndex)
	binary.LittleEndian.PutUint16(bs[22:24], h.ObjVersion)
	binary.LittleEndian.PutUint64(bs[24:32], h.Timestamp)
	return bs
}

// readBaseHeader from r.
// error != nil if base header is malformed.
func readBaseHeader(r io.Reader) (*objectHead, error) {

	bs := make([]byte, 32)
	if _, err := io.ReadFull(r, bs); err != nil {
		return nil, err
	}

	var head objectHead
	head.unmarshal(bs)

	if string(head.Signature[:]) != "LOBJ" {
		return nil, errors.New("ContainerReader out of sync with LOBJ markers!")
	}
	if head.Version != 1 {
		return nil, errors.New("Unsupported header version detected!")
	}

	return &head, nil
}

// IterObjects goes through blf objects in r.
func IterObjects(raw io.Reader) error {
	r := bufio.NewReader(raw)

	var err error
	var head *objectHead
	for {
		if head, err = readBaseHeader(r); err != nil {
			// Expected EOF
			if err == io.EOF {
				return nil
			} else {
				return err
			}
		}
		//fmt.Printf("%+v\n", head)
		// TODO: Time format is defined by head.Flags

		// Objects can contain extra bytes (more than padding).
		// We read potentially more than needed here.
		data := make([]byte, int(head.Objsize)-int(head.Headsize))
		_, err = io.ReadFull(r, data)
		if err != nil {
			return err
		}
		// regardless of object type/size, we must re-align
		if head.Objsize%4 != 0 {
			r.Discard(int(head.Objsize % 4))
		}

		// Read out actual message from one of many types
		// TODO: Handle more formats
		var msg CanMessage

		switch head.Objtype {
		case blfObjCanMessage:
			fallthrough
		case blfObjCanMessage2:
			msg.Channel = binary.LittleEndian.Uint16(data[0:2])
			msg.ID = binary.LittleEndian.Uint32(data[4:8])
			msg.Data = append([]byte(nil), data[8:16]...)

		default:
			continue // Skip to next
		}
		fmt.Printf("%+v\n", msg)
	}
	return nil
}

func CountObjects(raw io.Reader) (map[int]int, error) {
	r := bufio.NewReader(raw)

	counter := make(map[int]int)
	for {
		head, err := readBaseHeader(r)
		if err != nil {
			// Expected EOF
			if err == io.EOF {
				break
			} else {
				return nil, err
			}
		}
		// map get defaults to zero value
		counter[int(head.Objtype)] += 1

		// skip object bodies
		r.Discard(int(head.Objsize) - int(head.Headsize))
		if head.Objsize%4 != 0 {
			r.Discard(int(head.Objsize % 4))
		}
	}
	return counter, nil
}
