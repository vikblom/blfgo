package blfgo

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
)

// zip compresses a byte slice
func zip(data []byte) []byte {
	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(data)
	w.Close()
	return b.Bytes()
}

// Header for containers
type containerHead struct {
	// Typical fields for BLF
	// Signature shall always be LOBJ
	Signature [4]byte
	// Bytes in base+optinal extra header
	Headsize uint16
	// Version always 1
	Version uint16
	// Bytes in object
	Objsize uint32
	// Objtype is always 10 for containers
	Objtype uint32

	// Specific fields for containers
	Compressed   uint32 // double check this
	DeflatedSize uint32
} // 32 bytes

// marshal header into binary format.
func (h *containerHead) marshal() []byte {

	bs := make([]byte, 32)
	copy(bs[0:4], h.Signature[:])
	binary.LittleEndian.PutUint16(bs[4:6], h.Headsize)
	binary.LittleEndian.PutUint16(bs[6:8], h.Version)
	binary.LittleEndian.PutUint32(bs[8:12], h.Objsize)
	binary.LittleEndian.PutUint32(bs[12:16], h.Objtype)
	binary.LittleEndian.PutUint32(bs[16:20], h.Compressed)
	binary.LittleEndian.PutUint32(bs[24:28], h.DeflatedSize)
	return bs
}

// unmarshal a containerHeader from binary data.
func (h *containerHead) unmarshal(bs []byte) {
	copy(h.Signature[:], bs)
	h.Headsize = binary.LittleEndian.Uint16(bs[4:6])
	h.Version = binary.LittleEndian.Uint16(bs[6:8])
	h.Objsize = binary.LittleEndian.Uint32(bs[8:12])
	h.Objtype = binary.LittleEndian.Uint32(bs[12:16])
	h.Compressed = binary.LittleEndian.Uint32(bs[16:20])
	h.DeflatedSize = binary.LittleEndian.Uint32(bs[24:28])
}

// readContainerHead from r.
// error != nil if the header is malformed.
func readContainerHead(r io.Reader) (*containerHead, error) {

	bs := make([]byte, 32)
	if _, err := io.ReadFull(r, bs); err != nil {
		return nil, err
	}

	var h containerHead
	h.unmarshal(bs)

	// TODO: Log what header this is.

	// Sanity Checks
	if string(h.Signature[:]) != "LOBJ" {
		return nil, errors.New("ContainerReader out of sync with LOBJ markers!")
	}
	if h.Version != 1 {
		return nil, errors.New("Unsupported header version detected!")
	}
	if h.Objtype != 10 {
		return nil, errors.New("Unexpected object type!")
	}

	return &h, nil
}

// writeContainer writes a blob of data into w as a single "container".
// Compression is optional.
func writeContainer(w io.Writer, blob []byte, compressed bool) error {

	// TODO: Move this logic to marshal?
	compr_flag := uint32(0)
	if compressed {
		blob = zip(blob)
		compr_flag = 2
	}
	size := 32 + uint32(len(blob))

	head := &containerHead{
		Signature:  [4]byte{76, 79, 66, 74},
		Headsize:   16,
		Version:    1,
		Objsize:    size,
		Objtype:    10,
		Compressed: compr_flag}

	var err error
	// First the header
	_, err = w.Write(head.marshal())
	if err != nil {
		return err
	}
	// Then the body
	_, err = w.Write(blob)
	if err != nil {
		return err
	}
	// Finally padding
	_, err = w.Write(make([]byte, len(blob)%4))
	return err
}

type ContainerReader struct {
	buf []byte
	rd  *bufio.Reader
	// For stats
	read_bytes    uint
	lobjs_regular uint
	lobjs_zipped  uint
}

func NewContainerReader(rd io.Reader) *ContainerReader {
	cr := ContainerReader{
		buf: make([]byte, 0, 1024),
		rd:  bufio.NewReader(rd)}
	return &cr
}

func (cr *ContainerReader) String() string {
	return fmt.Sprintf("CR: %d read with %d in buf from %d+%d containers.",
		cr.read_bytes,
		len(cr.buf),
		cr.lobjs_regular,
		cr.lobjs_zipped)
}

// refill reads the next log container from the backed Reader.
//
// Only adds bytes to buf if err == nil.
// Decompresses the data if needed.
func (cr *ContainerReader) refill() error {

	cont, err := readContainerHead(cr.rd)
	if err != nil {
		return err
	}

	// Read container body
	// FIXME: buf is always empty if we get here.
	// FIXME, objsize includes header?
	tmp := make([]byte, cont.Objsize-32)
	_, err = io.ReadFull(cr.rd, tmp)
	if err != nil {
		return err
	}
	// Realign the Reader to the next LOBJ
	if cont.Objsize%4 != 0 {
		cr.rd.Discard(int(cont.Objsize % 4))
	}

	if cont.Compressed == 0 {
		cr.lobjs_regular++
	} else {
		cr.lobjs_zipped++

		// Let zlib decompress the payload
		// However, zlib might not need it all!
		// FIXME: Unread the bytes not in this object?
		byter := bytes.NewReader(tmp)
		unzipd, err := zlib.NewReader(byter)
		if err != nil {
			return err
		}
		defer unzipd.Close()
		tmp, err = io.ReadAll(unzipd)
		if err != nil {
			return err
		}
	}

	// actual fill
	cr.buf = tmp
	return nil
}

func (cr *ContainerReader) Read(p []byte) (int, error) {
	if len(p) == 0 {
		return 0, nil
	}

	for len(cr.buf) == 0 {
		err := cr.refill()
		if err != nil {
			return 0, err
		}
	}

	n := len(p)
	if len(cr.buf) < n {
		n = len(cr.buf)
	}

	// Move bytes into p, trim cr.buf
	copy(p[:n], cr.buf[:n])
	if len(cr.buf) > n {
		cr.buf = cr.buf[n:]
	} else {
		cr.buf = []byte{}
	}

	cr.read_bytes += uint(n)
	return n, nil
}
