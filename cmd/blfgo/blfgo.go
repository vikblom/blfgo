package main

import (
	"gitlab.com/vikblom/blfgo"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		println("Usage: conv file.blf")
		return
	}
	blfgo.ParseFile(os.Args[1])
}
