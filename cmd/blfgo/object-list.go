package main

import (
	"log"
	"os"

	"gitlab.com/vikblom/blfgo"
)

func main() {
	if len(os.Args) != 2 {
		println("Usage: conv file.blf")
		return
	}

	rdr, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal("Cannot not open file: ", os.Args[1], err)
	}
	defer rdr.Close()

	// BLF files have a header we must discard before consuming containers.
	if _, err := rdr.Seek(144, 0); err != nil {
		log.Fatal(err)
	}
	cr := blfgo.NewContainerReader(rdr)

	err = blfgo.IterObjects(cr)
	if err != nil {
		log.Fatal(err)
	}
}
