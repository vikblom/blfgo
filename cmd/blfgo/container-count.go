package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime/pprof"

	"gitlab.com/vikblom/blfgo"
)

var cpuprofile = flag.String("profile", "", "Write profiling data to file.")

func main() {

	flag.Parse()
	if flag.NArg() == 0 {
		flag.Usage()
		os.Exit(1)
	}

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	rdr, err := os.Open(flag.Arg(0))
	if err != nil {
		log.Fatal("Cannot not open file: ", os.Args[1], err)
	}
	defer rdr.Close()

	// BLF files have a header we must discard before consuming containers.
	if _, err := rdr.Seek(144, 0); err != nil {
		log.Fatal(err)
	}

	cr := blfgo.NewContainerReader(rdr)
	if _, err := io.ReadAll(cr); err != nil {
		log.Fatal(err)
	}
	fmt.Println(cr)
}
